import chap6.lambdas.Person;
import java.util.ArrayList;
import java.util.function.Predicate;

public class Main {

    public static final void main(String[] args) throws Throwable {
        Person p1 = new Person("Yvon",26,170);
        Person p2 = new Person("Yves",30,175);
        Person p3 = new Person("Lyne-sey",5,100);
        Person p4 = new Person("Natan",1,70);
        ArrayList<Person> people = new ArrayList<>();
        people.add(p1);
        people.add(p2);
        people.add(p3);
        people.add(p4);
        // lambda expression
        Predicate<Person> predicate = person -> person.getAge()>20;
        filter(people, predicate);

    }
    static void filter(ArrayList<Person> people, Predicate<Person> rule){
        for (Person p:people) {
            if (rule.test(p))
                System.out.println(p);
        }
    }

}
