package chap6.lambdas;

import chap6.lambdas.Person;
import chap6.lambdas.Validate;

public class ValidateWeightService implements Validate {
    @Override
    public boolean check(Person person) {
        return person.getWeight()>150;
    }
}
