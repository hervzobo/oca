package chap6.lambdas;

import chap6.lambdas.Person;

public interface Validate {
    boolean check(Person person);
}
