package chap6.lambdas;

import chap6.lambdas.Person;
import chap6.lambdas.Validate;

public class ValidateService implements Validate {
    //verifie si la person a plus de 20 ans
    @Override
    public boolean check(Person person) {
        return person.getAge()>20;
    }
}
